var mongoose = require('mongoose');

var schema = mongoose.Schema({
  name: 'string'
});

var User = mongoose.model('User', schema);

module.exports = User;
