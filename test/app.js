var app = require('../app'),
    assert = require('assert'),
    mongoose = require('mongoose'),
    request = require('supertest');

describe('app', function() {

  beforeEach(function(done) {
    mongoose.connection.db.executeDbCommand( { dropDatabase: 1 }, function(err, result) {
      done(err);
    });
  });

  it('GET /lookupWord?word=hack should return true', function(done) {
    request(app)
      .get('/lookupWord')
      .query({ word: 'hack' })
      .expect(200, { defined: true }, done);
  });

  it('GET /lookupWord?word=artlbartl should return false', function(done) {
    request(app)
      .get('/lookupWord')
      .query({ word: 'artlbartl' })
      .expect(200, { defined: false }, done);
  });

  it('GET /lookupWord?word=unfound should return false', function(done) {
    request(app)
      .get('/lookupWord')
      .query({ word: 'unfound' })
      .expect(200, { defined: false }, done);
  });

  it('GET /newGame should return a board', function(done) {
    request(app)
      .get('/newGame')
      .expect(200)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }

        assert(res.body.board.match(/^[A-Z]{25}$/));

        done();
      });
  });

  it('GET /checkMove with a valid move/word', function(done) {
    request(app)
      .get('/checkMove')
      .query({ board: 'CCCWIAECHGKZHSGHFEWJNKIUJ', word: 'HACK' })
      .expect(200, { valid: true }, done);
  });

  it('GET /checkMove with a valid move/ invalid word', function(done) {
    request(app)
      .get('/checkMove')
      .query({ board: 'CCCWIARTLBARTLFEWJNKIUJAA', word: 'ARTLBARTL' })
      .expect(200, { valid: false }, done);
  });

  it('GET /checkMove with a invalid move/ valid word', function(done) {
    request(app)
      .get('/checkMove')
      .query({ board: 'CCCWIAECQGKZQSGQFEWJNKIUJ', word: 'HACK' })
      .expect(200, { valid: false }, done);
  });

  it('GET /checkMove with a invalid move/ invalid word', function(done) {
    request(app)
      .get('/checkMove')
      .query({ board: 'CCCWIAECHGKZHSGHFEWJNKIUJ', word: 'ARTLBARTL' })
      .expect(200, { valid: false }, done);
  });

  it('GET /hello should return 200', function(done) {
    request(app)
      .get('/hello')
      .expect(200, { hello: "world" }, done);
  });

  it('POST /hello should return 200/201', function(done) {
    request(app)
      .post('/hello')
      .send({ name: 'hackreactor' })
      .expect(201)
      .end(function(err, res) {
        if (err) {
          return done(err);
        }

        assert.equal('hackreactor', res.body.name);

        request(app)
          .post('/hello')
          .send({ name: 'hackreactor' })
          .expect(200)
          .end(function(err, res) {
            if (err) {
              return done(err);
            }

            assert.equal('hackreactor', res.body.name);

            done();
          });
      });
  });
});
