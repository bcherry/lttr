var express = require('express'),
    mongoose = require('mongoose'),
    request = require('request'),
    cheerio = require('cheerio'),
    User = require('./models/User');

mongoose.connect('localhost', 'lttr-' + process.env.NODE_ENV);

var app = express();

app.use(express.bodyParser());

function validateWord(word, callback) {
  request.get('http://definr.com/' + word, function(err, response, body) {
    if (err) {
      return callback(err);
    }

    var $ = cheerio.load(body);

    var meaning = $("#meaning").text();

    callback(null, !meaning.match(/^(Did you mean\:)|(Not found)/));
  });
}

app.get('/lookupWord', function(req, res, next) {
  validateWord(req.query.word, function(err, defined) {
    if (err) {
      return next(err);
    }

    res.send(200, { defined: defined });
  });
});

app.get('/newGame', function(req, res, next) {
  var board = '';

  for (var i = 0; i < 25; i++) {
    board += String.fromCharCode(Math.floor(Math.random() * 26) + 'A'.charCodeAt(0))
  }

  res.send(200, { board: board });
});

app.get('/checkMove', function(req, res, next) {
  var board = req.query.board.split(''),
      word = req.query.word.split('');

  for (var i = 0; i < word.length; i++) {
    var index = board.indexOf(word[i]);
    if (index === -1) {
      return res.send(200, { valid: false });
    }

    board.splice(index, 1);
  }

  validateWord(req.query.word, function(err, defined) {
    if (err) {
      return next(err);
    }

    if (!defined) {
      return res.send(200, { valid: false });
    }

    res.send(200, { valid: true });
  });

});

app.get('/hello', function(req, res, next) {
  res.send(200, { hello: "world" });
});

app.post('/hello', function(req, res, next) {
  User.findOne({ name: req.param('name') }, function(err, user) {
    if (err) {
      return next(err);
    }

    if (!user) {
      new User({ name: req.param('name') }).save(function(err, user) {
        if (err) {
          return next(err);
        }

        res.send(201, user);
      });
    } else {
      res.send(200, user);
    }

  });
});

if (process.env.NODE_ENV !== 'test') {
  app.listen(3000);
  console.log('Listening on port 3000');
}

module.exports = app;
